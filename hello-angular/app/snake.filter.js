const snakeFilter = function() {
  return function (input) {
    return input.replace(/-/g , "_");
  };
};

export default snakeFilter;
