import angular from 'angular'
import angularMaterialize from 'angular-materialize'
import kebabFilter from 'kebab.filter'
import snakeFilter from 'snake.filter'
import camelFilter from 'camel.filter'
import piglatinFilter from 'piglatin.filter'


angular.module('my-app', [angularMaterialize])
  .filter('kebab', kebabFilter)
  .filter('snake', snakeFilter)
  .filter('camel', camelFilter)
  .filter('piglatin', piglatinFilter);
