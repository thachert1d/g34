// split the word into an array
// for each word
// are the first two letters consonants?
// if yes, move them to the end
// if not is the first letter a consonant?
// if yes, move that letter to the end
// else just add the "ay" to the end of the word
//
// at "ay" to the end of the word

const piglatinFilter = function() {
  return function (input) {
    let output = input.split(' ');
    for ( var i = 0; i < output.length - 1; i++ )
      if ((output[i][0] && output[i][1]) !== 'a' | 'e' |'i' | 'o' | 'u' | 'y' ) {
        return output = output[i].slice(2) + output[i][0] + 'ay';
      } else if ((output[i][0]) !== 'a' | 'e' |'i' | 'o' | 'u' | 'y' ) {
        return output = output[i].slice(1) + output[i][0] + output[i][1] + 'ay';
      } else {
        return output + 'ay'
      }
  }
};

export default piglatinFilter;
