const camelFilter = function() {
  return function (input) {
    let inputToCamel = input.replace(/-/g , "_");
    let words = inputToCamel.split('_');

      for ( var i = 1; i < words.length; i++ )
        words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
        return words.join('');

        return input;
      };
    };

export default camelFilter;
