const kebabFilter = function() {
  return function (input) {
    return input.replace(/_/g , "-");
  };
};

export default kebabFilter;
