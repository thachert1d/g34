import angular from 'angular'

class HomeCtrl {
  constructor($http) {
    $http.get(`http://www.omdbapi.com/?s=batman`)
      .then((response) => {
        console.log(response.data.Search);
        this.movies = (response.data.Search);
      })
      .catch((error) => {
        console.error(error);
      })
  }
}

angular.module('my-app', [])
  .controller('homeCtrl', HomeCtrl)

  // .controller('homeCtrl', function($http){
  //     const vm = this;
  //     $http.get(`http://www.omdbapi.com/?s=batman`).then(
  //       function(response) {
  //         console.log(response.data.Search);
  //         vm.movies = response.data.Search;
  //       }
  //     )
  //   }
  // )
  // .controller('homeCtrl', function($http){
  //     $http.get(`http://www.omdbapi.com/?s=batman`).then(
  //       (response) => {
  //         console.log(response.data.Search);
  //         this.movies = response.data.Search;
  //       }
  //     )
  //   }
  // )

  //
  //
  //
  // function foo($http){
  //     $http.get(`http://www.omdbapi.com/?s=batman`).then(
  //       (response) => {
  //         console.log(response.data.Search);
  //         this.movies = response.data.Search;
  //       }
  //     )
  //   }
  //
  import angular from 'angular'

  class HomeService {
    constructor($http) {
      this.http = $http;
    }

    getMovies() {
      return this.http.get(`http://www.omdbapi.com/?s=batman`).then(
        (response) => {
          return response.data.Search;
        }
      )
    }
  }

  class HomeCtrl {
    constructor(HomeService) {
      HomeService.getMovies().then(
        (movies) => {
          this.movies = movies;
        }
      )
    }
  }

  angular.module('my-app', [])
    .service('HomeService', HomeService)
    .controller('homeCtrl', HomeCtrl)
