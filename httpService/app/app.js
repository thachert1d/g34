import angular from 'angular'

class HomeService {
  constructor($http) {
    this.$http = $http;
  }

  getMovies() {
    return this.$http.get(`http://www.omdbapi.com/?s=batman`).then(
      (response) => {
        return response.data.Search;
      }
    )
  }
}

class HomeCtrl {
  constructor(HomeService) {
    HomeService.getMovies().then(
      (movies) => {
        this.movies = movies;
      }
    )
  }
}

HomeService.$inject = ['$http'];

angular.module('my-app', [])
  .controller('HomeCtrl', HomeCtrl)
  .service('HomeService', HomeService)
